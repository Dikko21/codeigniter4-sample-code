<?php

namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		return view('welcome_message');
	}

	public function index2()
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'http://ymmi.aiseeyou.tech/api/dashboard/detailstatistic',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => '{
    "workflow_code":"w-asm-ag3-001"
}',
			CURLOPT_HTTPHEADER => array(
				'Authorization: Basic bmljYWJyZW9uOnBhc3N3b3Jk',
				'Content-Type: application/json'
			),
		));

		$response = json_decode(curl_exec($curl));

		curl_close($curl);

		$data = ['api' => $response];

		return view('contoh', $data);
	}
}
